﻿using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MatchStatistic
{
    public List<Match> matches = new List<Match>();

    public int ID;
    public int Wins;
    public int Draw;
    public int Loses; 

    public void ManageMatchResult(int goals, int missed)
    {
        if (goals == missed)
        {
            Draw++;
        }
        else if(goals > missed)
        {
            Wins++;
        }
        else if(goals < missed)
        {
            Loses++;
        }
    }

    public int GetGoals()
    {
        int goals = 0;
        for (int i = 0; i < matches.Count; i++)
        {
            goals += matches[i].GoaledBalls;
        }
        return goals;
    }

    public int GetWins()
    {
        return Wins;
    }

    public int GetBalls(int score, bool isGoals)
    {
        int balls = 0;
        for (int i = 0; i < matches.Count; i++)
        {
            if(isGoals)
            { 
                if(matches[i].GoaledBalls == score)
                { 
                    balls += 1;
                }
            }
            else
            {
                if (matches[i].MissedBalls == score)
                {
                    balls += 1;
                }
            }
        }
        return balls;
    }
}
