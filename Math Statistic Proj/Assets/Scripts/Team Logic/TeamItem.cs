﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class TeamItem : MonoBehaviour {

    private RectTransform rectTransform;
    private Image sprite;

    private Coroutine CompressionCoroutine;
    private Coroutine DecompressionCoroutine; 

    private TeamManager teamManager;
    private MatchStatistic matchStatistic;
    private XMLManager managerXML;

    private bool choosed = false;

    #region ITEMS CHARACTERISTIC
    [SerializeField]
    private string teamName; 
    private int wins; 
    public int ID;
    #endregion

    private void Awake()
    { 
        teamManager = TeamManager.Instance;

        sprite = GetComponent<Image>();
        rectTransform = GetComponent<RectTransform>(); 
    }

    private void Start()
    {
        managerXML = FindObjectOfType<XMLManager>();
        matchStatistic = managerXML.matchStatistics[ID];
        SyncData();
    }

    private void EnableShadow()
    {
        if (choosed == false)
        {
            choosed = true;
            sprite.color = new Color(sprite.color.r, sprite.color.b, sprite.color.g, 0.25f);
        }
        else
        {
            choosed = false;
            sprite.color = new Color(sprite.color.r, sprite.color.b, sprite.color.g, 1);
        }
    }

    public void OnTap()
    {
        if(TeamManager.Instance.CheckForAviableSpace())
        {
            EnableShadow();
            TeamManager.Instance.GetAviableSpace(sprite, teamName);
            TeamManager.Instance.CalculateSliderValue();
            RemoveFromGroup();
            TeamTextControll(); // BAD DESIGN!
        } 
    }

    public void SyncData()
    { 
        wins = matchStatistic.GetWins(); 
    }

    private void TeamTextControll()
    {
        if (!TeamManager.Instance.CheckForAviableSpace())
        {
            UIController.Instance.SetBack1Text("SCROLL DOWN");
        }
        else
        {
            UIController.Instance.SetBack1Text("CHOOSE TEAM");
        }
    }

    public string GetName()
    {
        return teamName;
    }

    public int GetPower()
    {
        return wins;
    }

    private void RemoveFromGroup()
    {
        if (DecompressionCoroutine != null)
            StopCoroutine(DecompressionCoroutine);

        CompressionCoroutine = StartCoroutine(CompressItem());
    }

    public void RestroreToGroup()
    { 
        TeamTextControll();

        if (CompressionCoroutine != null)
            StopCoroutine(CompressionCoroutine);
        if (teamManager.isOpen)
            DecompressionCoroutine = StartCoroutine(DecompressItem());
        else
            ImmediatelyResoreToGroup();
    }

    private void ImmediatelyResoreToGroup()
    {
        rectTransform.sizeDelta = new Vector2(180f, 180f);
        EnableShadow();
    }

    private IEnumerator CompressItem()
    {  
        float startTime = Time.time;
        float overTime = 1;
        float totalTime = Time.time + 1f;

        Vector2 startPoint = new Vector2(180f, 180f);
        Vector2 endPoint = new Vector2(-335f, 180f);

        while (Time.time < totalTime)
        { 
            rectTransform.sizeDelta = Vector2.Lerp(rectTransform.sizeDelta, endPoint, (Time.time - startTime) / overTime);
            yield return null;
        } 
    }

    private IEnumerator DecompressItem()
    {  
        float startTime = Time.time;
        float overTime = 1;
        float totalTime = Time.time + 1f;

        Vector2 startPoint = new Vector2(180f, 180f);
        Vector2 endPoint = new Vector2(-335f, 180f);

        Color endColor = new Color(sprite.color.r, sprite.color.b, sprite.color.g, 1);

        while (Time.time < totalTime)
        {
            rectTransform.sizeDelta = Vector2.Lerp(rectTransform.sizeDelta, startPoint, (Time.time - startTime) / overTime);
            sprite.color = Color.Lerp(sprite.color, endColor, (Time.time - startTime) / overTime);
            yield return null;
        }
        EnableShadow();
    }
}

