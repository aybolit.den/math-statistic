﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamManager : MonoBehaviour {

    #region SINGLETON
    private static TeamManager instance = null;
    public static TeamManager Instance
    {
        get
        {
            if (instance == null)
            {
                TeamManager obj = FindObjectOfType<TeamManager>();
                if (obj == null)
                {
                    instance = new GameObject(typeof(TeamManager).ToString(), typeof(TeamManager)).GetComponent<TeamManager>();
                }
                else
                {

                }
                {
                    instance = obj;
                }
            }
            return instance;
        }
        private set
        {
            if (instance == null && value.GetComponent<TeamManager>() != null)
            {
                instance = value;
            }
        }
    }
    #endregion

    private UIController uiController;
    private CameraMovement cameraMovement;
    [SerializeField]
    BarChart[] barCharts;

    // Cashed Items
    [SerializeField]
    private TeamItem cashedTeam1;
    [SerializeField]
    private TeamItem cashedTeam2;

    public bool isOpen { get; set; }
    private bool hasAviableSpace { get; set; }
    private bool firstAviable = true;
    private bool secondAviable = true;

    void Awake()
    {
        Instance = this;
        uiController = UIController.Instance;
        cameraMovement = CameraMovement.Instance;
    }

    public bool CheckForAviableSpace()
    {
        if (firstAviable || secondAviable)
        {
            hasAviableSpace = true;
        }
        else
        {
            hasAviableSpace = false;
        }

        return hasAviableSpace;
    }

    public void BuildBitChart()
    {
        if (cashedTeam1 != null && cashedTeam2 != null)
        {
            cameraMovement.ForceNextSlide(); 
            XMLManager xMLManager = FindObjectOfType<XMLManager>();
            barCharts[0].matchStatistic = xMLManager.matchStatistics[cashedTeam1.ID];
            barCharts[1].matchStatistic = xMLManager.matchStatistics[cashedTeam2.ID];
            barCharts[0].BuildGraphs();
            barCharts[1].BuildGraphs();

            uiController.SetTeam1BarChartText(cashedTeam1.GetName());
            uiController.SetTeam2BarChartText(cashedTeam2.GetName());
        }
    }

    public void OpenTeamGroup() // OnButtonClick
    { 
        uiController.SetActiveTeamGroup(true);
        cameraMovement.LetMoveCamera(false);
        cameraMovement.MoveToLastPointImmediately();
        isOpen = true;
    }

    public void CloseTeamGroup() // OnButtonClick
    {
        uiController.SetActiveTeamGroup(false); 
        cameraMovement.LetMoveCamera(true);
        isOpen = false;
    } 

    public void GetAviableSpace(UnityEngine.UI.Image newFootballGerb, string name)
    {
        if (firstAviable)
        {
            uiController.placeImage1.sprite = newFootballGerb.sprite;
            uiController.placeQuestion1.SetActive(false);
            uiController.placeText1.text = name;

            cashedTeam1 = newFootballGerb.gameObject.GetComponent<TeamItem>();
            firstAviable = false;
        }
        else if (secondAviable)
        {
            uiController.placeImage2.sprite = newFootballGerb.sprite;
            uiController.placeQuestion2.SetActive(false);
            uiController.placeText2.text = name;

            cashedTeam2 = newFootballGerb.gameObject.GetComponent<TeamItem>(); 
            secondAviable = false;
        } 
    }

    public void CalculateSliderValue()
    {
        float first = 0;
        float second = 0;

        if (!ReferenceEquals(cashedTeam1, null))
        {
            first = (float)cashedTeam1.GetPower();
        }
        if(!ReferenceEquals(cashedTeam2, null))
        {
           second = (float)cashedTeam2.GetPower();
        } 
        SliderController.Instance.UpdateValue(first, second);
    }

    public void ResetDefaultTexture(int ind)
    {
        if(ind == 1 && !ReferenceEquals(cashedTeam1, null))
        {
            uiController.placeImage1.sprite = uiController.placeDefaultSprite1;
            uiController.placeQuestion1.SetActive(true);
            uiController.placeText1.text = "TEAM I";

            firstAviable = true;
            cashedTeam1.RestroreToGroup();
            cashedTeam1 = null;
        }
        else if(ind == 2 && !ReferenceEquals(cashedTeam2, null))
        {
            uiController.placeImage2.sprite = uiController.placeDefaultSprite2;
            uiController.placeQuestion2.SetActive(true);
            uiController.placeText2.text = "TEAM II";

            secondAviable = true;
            cashedTeam2.RestroreToGroup();
            cashedTeam2 = null;
        }
        TeamManager.Instance.CalculateSliderValue();
    }   
}
