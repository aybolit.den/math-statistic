﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization; 
using UnityEngine;

public class XMLManager : MonoBehaviour {

    // By Context menu  
    public List<MatchStatistic> matchStatistics =  new List<MatchStatistic>();

    private void Awake()
    {
        ManageMatchID();
        LoadData();
    } 

    public void LoadData()
    {
        for (int i = 0; i < 10; i++)
        {
            XmlSerializer serializer = new XmlSerializer(typeof(MatchStatistic));
            FileStream stream = new FileStream(Application.dataPath + "/Scripts/XML Data Base/DataBase" + i.ToString() + ".xml", FileMode.Open);
            matchStatistics[i] = serializer.Deserialize(stream) as MatchStatistic;
            stream.Close();
        }
    }

    [ContextMenu("RUN:: SAVE DATA")]
    public void SaveData()
    { 
        for (int i = 0; i < 10; i++)
        { 
            XmlSerializer serializer = new XmlSerializer(typeof(MatchStatistic));
            FileStream stream = new FileStream(Application.dataPath + "/Scripts/XML Data Base/DataBase" + i.ToString() + ".xml", FileMode.Create);
            serializer.Serialize(stream, matchStatistics[i]);
            stream.Close();
        }
    }

    [ContextMenu("RUN:: CREATE DATA")]
    public void CreateData()
    {
        for (int u = 0; u < matchStatistics.Count; u++)
        {
            int goals = ManageLeagueCharacteristic(GetLeague(), true);
            int missed = ManageLeagueCharacteristic(GetLeague(), false);
            for (int v = 0; v < 100; v++)
            {
                matchStatistics[u].matches.Add(new Match());
                matchStatistics[u].matches[v].MissedBalls = Random.Range(0, missed);
                matchStatistics[u].matches[v].GoaledBalls = Random.Range(0, goals);
                matchStatistics[u].ManageMatchResult(matchStatistics[u].matches[v].GoaledBalls, matchStatistics[u].matches[v].MissedBalls);
            }
        }
    }

    private void ManageMatchID()
    {
        for (int i = 0; i < 10; i++)
        {
            matchStatistics.Add(new MatchStatistic());
            matchStatistics[i].ID = i;
        }
    }

    private int ManageLeagueCharacteristic(League league, bool isGoals)
    {
        if(isGoals)
        {
            switch (league)
            {
                case League.Easy: return 2;

                case League.Medium: return 3;

                case League.Hard: return 5;
            }
            return 0; 
        }
        else
        {
            switch (league)
            {
                case League.Easy: return 5;

                case League.Medium: return 3;

                case League.Hard: return 2;
            }
            return 0;
        }
    }

    private League GetLeague()
    {
        switch (Random.Range(0, 3))
        {
            case 0: return League.Easy; 

            case 1: return League.Medium;

            case 2: return League.Hard;

            default: return League.Medium;
        }
    }
}

enum League
{
    Easy, 
    Medium, 
    Hard
}
