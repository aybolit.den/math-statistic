﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Bar : MonoBehaviour {

    [SerializeField]
    private Image barImage;
    [SerializeField]
    private Text variant;
    private RectTransform rectTransform;

    public void BuildBar(float max, int value)
    {
        rectTransform = GetComponent<RectTransform>();
        StartCoroutine(BuildingBar(max));
        variant.text = value + "";
    }

    private IEnumerator BuildingBar(float max)
    {
        float startTime = Time.time;
        float overTime = 5f;
        float totalTime = Time.time + 5f;

        Vector2 startPoint = new Vector2(100f, 1f);
        Vector2 endPoint = new Vector2(100f, max);

        while (Time.time < totalTime)
        {
            rectTransform.sizeDelta = Vector2.Lerp(rectTransform.sizeDelta, endPoint, (Time.time - startTime) / overTime);
            yield return null;
        }
    }
}
