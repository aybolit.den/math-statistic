﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using UnityEngine;

public class BarChart : MonoBehaviour {

    [SerializeField]
    private GameObject barPrefab;
    private TeamManager teamManager;
    public MatchStatistic matchStatistic;

    private List<Bar> bars = new List<Bar>();
    [SerializeField]
    private List<int> balls = new List<int>();
    private int[] values = { -4, -3, -2, -1, 0, 1, 2, 3, 4 };

    private void Start()
    {
        CreateBars();
    }

    private void CreateBars()
    {
        for (int i = 0; i < values.Length; i++)
        {
            GameObject bar = Instantiate(barPrefab, transform);
            bars.Add(bar.GetComponent<Bar>());
        }
    }

    private void PutData()
    {
        for (int i = 0; i < values.Length; i++)
        {
            if (i == 0) // first
            {
                int ballCount = 0;
                ballCount = matchStatistic.GetBalls(4, false); 
                balls.Add(ballCount);
            }
            else if (i == values.Length - 1) // last
            {
                int ballCount = 0;
                ballCount = matchStatistic.GetBalls(4, true); 
                balls.Add(ballCount);
            }
            else // others
            {
                if (values[i] < 0)
                {
                    balls.Add(matchStatistic.GetBalls(-1 * values[i], false)); 
                }
                else
                {
                    balls.Add(matchStatistic.GetBalls(values[i], true));
                }
            }
        }
    }

    public void BuildGraphs()
    {
        balls.Clear();
        PutData();
        int maxBValue = balls.Max();
        for (int i = 0; i < values.Length; i++)
        {
            bars[i].BuildBar(Proportion(balls[i], maxBValue), balls[i]);
        }
    }

    private float Proportion(float value, float Max)
    {
        return (value * 800) / Max;
    }
}
