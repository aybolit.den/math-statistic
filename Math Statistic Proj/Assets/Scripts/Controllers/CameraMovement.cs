﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    #region SINGLETON
    private static CameraMovement instance = null;
    public static CameraMovement Instance
    {
        get
        {
            if (instance == null)
            {
                CameraMovement obj = FindObjectOfType<CameraMovement>();
                if (obj == null)
                {
                    instance = new GameObject(typeof(CameraMovement).ToString(), typeof(CameraMovement)).GetComponent<CameraMovement>();
                }
                else
                {

                }
                {
                    instance = obj;
                }
            }
            return instance;
        }
        private set
        {
            if (instance == null && value.GetComponent<CameraMovement>() != null)
            {
                instance = value;
            } 
        }
    }
    #endregion

    private ApplicationController applicationController;

    [SerializeField]
    private Vector3[] targets;
    private Vector3 targetPos;
    private Vector3 startPos;
    private Camera cam;

    [SerializeField]
    private float cameraSpeed = 0.058f;
    [SerializeField]
    private float slideDistance;
    private float pos;

    private bool allowMove = true;
    private int targetInd = 0;

    void Awake()
    {
        Instance = this;
        cam = GetComponent<Camera>();
        applicationController = ApplicationController.Instance; 
        targetPos = transform.position;  
    }

    void Update()
    {
        if(allowMove)
        { 
            if (Input.GetMouseButtonDown(0))
            {
                startPos = cam.ScreenToWorldPoint(Input.mousePosition);
            }
            else if (Input.GetMouseButton(0))
            {
                pos = cam.ScreenToWorldPoint(Input.mousePosition).y - startPos.y; 
                targetPos.y = transform.position.y - pos; 
                targetPos.x = 0;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if(pos > 0)
                {
                    if (Mathf.Abs(pos) > slideDistance)
                    {
                        if (targetInd < targets.Length -1)
                        {
                            targetInd++;
                        }
                    }
                    MoveToSlide();
                }
                else
                {
                    if (Mathf.Abs(pos) > slideDistance)
                    {
                        if(targetInd >= 1)
                        { 
                            targetInd--;
                        }
                    }
                    MoveToSlide();
                }
            }
            cam.transform.position = Vector3.Lerp(transform.position, targetPos, cameraSpeed * Time.deltaTime);
        }
    }

    private void MoveToSlide()
    { 
        targetPos = targets[targetInd];
        applicationController.OnSlideMove(targetInd);
    }

    public void ForceNextSlide()
    {
        targetPos = targets[++targetInd];
        applicationController.OnSlideMove(targetInd);
    }

    public void LetMoveCamera(bool value)
    {
        allowMove = value;
    }

    public void MoveToLastPointImmediately()
    {
        targetPos = targets[targetInd];
        cam.transform.position = targets[targetInd];  
    }
}
