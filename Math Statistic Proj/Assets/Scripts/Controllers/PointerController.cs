﻿using UnityEngine.EventSystems;
using UnityEngine;

public class PointerController : MonoBehaviour {

    private TeamManager teamManager; 

    private void Awake()
    {
        teamManager = TeamManager.Instance;    
    } 

    public void Onclick()
    {
        if (teamManager.isOpen)
        {
            teamManager.CloseTeamGroup();
        }
    }
}
