﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIController : MonoBehaviour {

    #region SINGLETON
    private static UIController instance = null;
    public static UIController Instance
    {
        get
        {
            if (instance == null)
            {
                UIController obj = FindObjectOfType<UIController>();
                if (obj == null)
                {
                    instance = new GameObject(typeof(UIController).ToString(), typeof(UIController)).GetComponent<UIController>();
                }
                else
                {

                }
                {
                    instance = obj;
                }
            }
            return instance;
        }
        private set
        {
            if (instance == null && value.GetComponent<UIController>() != null)
            {
                instance = value;
            } 
        }
    }
    #endregion 

    [SerializeField]
    private Animation pickTeamsAnimation;
    [SerializeField]
    private GameObject teamGroupUI;
    public Text chooseTeamText;
    public Text textNum;
    #region SLIDER UI 
    public Image sliderBackgroundImage;
    public Color sliderBackgroundColor;
    #endregion

    #region BAR CHART TEAM NAME 
    [SerializeField]
    private Text teamText1;
    [SerializeField]
    private Text teamText2;
    #endregion

    #region Team Manager UI
    // Image Places 
    public Image placeImage1; 
    public Image placeImage2;
    // Text Places
    public Text placeText1;
    public Text placeText2; 
    // Sprites Places
    public Sprite placeDefaultSprite1;
    public Sprite placeDefaultSprite2;
    // Questions Places
    public GameObject placeQuestion1;
    public GameObject placeQuestion2;
    #endregion 

    void Awake()
    {
        Instance = this; 
    }

    public void SetTeam1BarChartText(string name)
    {
        teamText1.text = name;
    }

    public void SetTeam2BarChartText(string name)
    {
        teamText2.text = name;
    }

    public void SetBack1Text(string value)
    {
        chooseTeamText.text = value;
    }

    public void SetActiveTeamGroup(bool value)
    {
        teamGroupUI.SetActive(value);
    }

    public void EnablePickTeamText(bool value)
    { 
        if(value)
        {
            pickTeamsAnimation.Play();
        }
        else
        {
            pickTeamsAnimation.Stop(); 
        }
        pickTeamsAnimation.enabled = value; 
    }
}
