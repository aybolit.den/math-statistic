﻿using System.Collections;
using System.Collections.Generic;   
using UnityEngine;

public class ApplicationController : MonoBehaviour {

    #region SINGLETON
    private static ApplicationController instance = null;
    public static ApplicationController Instance
    {
        get
        {
            if (instance == null)
            {
                ApplicationController obj = FindObjectOfType<ApplicationController>();
                if (obj == null)
                {
                    instance = new GameObject(typeof(ApplicationController).ToString(), typeof(ApplicationController)).GetComponent<ApplicationController>();
                }
                else
                {

                }
                {
                    instance = obj;
                }
            }
            return instance;
        }
        private set
        {
            if (instance == null && value.GetComponent<ApplicationController>() != null)
            {
                instance = value;
            } 
        }
    }
    #endregion

    private UIController uIController;
    private TeamManager teamManager;

    private void Awake()
    {
        uIController = UIController.Instance;
        teamManager = TeamManager.Instance;
    }

    public void OnSlideMove(int slideNumber)
    {
        switch (slideNumber)
        {
            case 0:
                FirstSlide();
                break;
            case 1:
                Secondlide();
                break;
            case 2:
                ThirdSlide();
                break;
            case 3:
                FourthSlide();
                break;
            case 4:
                FifthSlide();
                break;

            default:
                break;
        }
    }

    #region Slides
    private void FirstSlide()
    {
        uIController.EnablePickTeamText(false);
        teamManager.CloseTeamGroup();
    }

    private void Secondlide()
    {
        uIController.EnablePickTeamText(true);
    }

    private void ThirdSlide()
    {
        uIController.EnablePickTeamText(false);
        teamManager.CloseTeamGroup();
    }

    private void FourthSlide()
    {
 
    }

    private void FifthSlide()
    { 

    }
    #endregion
}
