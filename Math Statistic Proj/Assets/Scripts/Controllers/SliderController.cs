﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SliderController : MonoBehaviour {

    #region SINGLETON
    private static SliderController instance = null;
    public static SliderController Instance
    {
        get
        {
            if (instance == null)
            {
                SliderController obj = FindObjectOfType<SliderController>();
                if (obj == null)
                {
                    instance = new GameObject(typeof(SliderController).ToString(), typeof(SliderController)).GetComponent<SliderController>();
                }
                else
                {

                }
                {
                    instance = obj;
                }
            }
            return instance;
        }
        private set
        {
            if (instance == null && value.GetComponent<SliderController>() != null)
            {
                instance = value;
            }
        }
    }
    #endregion 

    private Scrollbar scrollbar;
    private UIController uIController;

    private void Awake()
    {
        scrollbar = GetComponent<Scrollbar>();
        uIController = UIController.Instance;
    }

    public void UpdateValue(float first, float second)
    {
        StartCoroutine(CalculateValues(first, second));
    }

    private IEnumerator CalculateValues(float first, float second)
    {
        Color color;
        float startTime = Time.time;
        float overTime = 1.5f;
        float totalTime = Time.time + 1f;
        float endPoint;

        if (first + second == 0)
        {
            endPoint = first / (100);
            color = Color.white;
        }
        else
        { 
            endPoint = first / (first + second);
            color = uIController.sliderBackgroundColor;
        }
        int left = (int)(endPoint * 100); 

        uIController.textNum.text = left + "% / " + (100 - left) + "%";

        while (Time.time < totalTime)
        {
            scrollbar.size = Mathf.Lerp(scrollbar.size, endPoint, (Time.time - startTime) / overTime);
            uIController.sliderBackgroundImage.color = Color.Lerp(uIController.sliderBackgroundImage.color, color, (Time.time - startTime) / overTime);

            yield return null;
        }
    }
}
